FROM node:10.15.3-jessie
VOLUME ./:/usr/src/mutant
WORKDIR /usr/src/mutant
COPY package.json package-lock.json ./
RUN npm install
COPY . .
EXPOSE 8080
ENV PORT=8080
CMD [ "npm", "start" ]

#docker run -d -p 8080:8080 --name mutant mutant
