# MUTANT - DESAFIO
Esta documentação visa explicar como rodar o meu projeto, bem como explicar as funcionalidades nele contidas.

**Primeiros passos:**

1. Para poder montar o ambiente do projeto e rodar, rode no terminal:

    `docker-compose up`

    Lembrando que para isso você precisa estar na pasta do projeto

PS: Necessário docker na máquina

A documentação da API está disponivel aqui - https://documenter.getpostman.com/view/4241275/S1Lr4WnB

**Funcionalidades**



_Todas os resultados são cacheados após a primeira consulta por 5 minutos)_

1. Endpoint para trazer os websites de todos os usuários - /api/sites
2. Endpoint para listar todos os usuários, este mesmo permite a ordenação por campo (name, email, company) - /api/users?filter=name;
3. Endpoint para listar os usuários que tem Suites - /api/users/suite

PS2: Não implementei elasticsearch pois não tive tempo, mas caso seja necessário posso desenvolver

Quando terminar, peço a gentileza de me passar um feedback sobre o projeto em si e sobre o meu código.

Fico no aguardo.

Best regards,

Cleiton.