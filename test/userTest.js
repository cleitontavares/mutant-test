const chai = require("chai");
const chaiHttp = require('chai-http');

chai.use(chaiHttp);

const expect = chai.expect;
const url =  'http://localhost:8080/api';

describe('Get data from data source', () => {

	it('All users without order', (done) => {
		chai.request(url)
			.get('/users')
			.end((err, response) => {
				expect(!err); // Expected to not return an error
				expect(response.statusCode).to.equal(200); // Expected to have a status of 200
				expect(response.body).to.have.property('data'); // You are expected to have the date property
				expect(response.body.data).to.be.an('array'); // It is expected to have an array within the data
				done();
			});
	});

	it('All users websites', (done) => {
		chai.request(url)
			.get('/sites')
			.end((err, response) => {
				expect(!err); // Expected to not return an error
				expect(response.statusCode).to.equal(200); // Expected to have a status of 200
				expect(response.body).to.have.property('data'); // You are expected to have the date property
				expect(response.body.data).to.be.an('array'); // It is expected to have an array within the data
				if(response.body.data.length){
					expect(response.body.data[0]).to.be.an('string'); // Expected to contain a string
					function validURL(str) {
						let pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
							'((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
							'((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
							'(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
							'(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
							'(\\#[-a-z\\d_]*)?$','i'); // fragment locator
						return !!pattern.test(str);
					}
					expect(validURL(response.body.data[0])).to.be.true; // And this string is a url
				}
				done();
			});
	});

	it('All users with suite', (done) => {
		chai.request(url)
			.get('/users/suite')
			.end((err, response) => {
				expect(!err); // Expected to not return an error
				expect(response.statusCode).to.equal(200); // Expected to have a status of 200
				expect(response.body).to.have.property('data'); // You are expected to have the date property
				expect(response.body.data).to.be.an('array'); // It is expected to have an array within the data
				done();
			});
	});

	it('All users order by name', (done) => {
		chai.request(url)
			.get('/users?filter=name')
			.end((err, response) => {
				expect(!err); // Expected to not return an error
				expect(response.statusCode).to.equal(200); // Expected to have a status of 200
				expect(response.body).to.have.property('data'); // You are expected to have the date property
				expect(response.body.data).to.be.an('array'); // It is expected to have an array within the data
				done();
			});
	});

});