const axios = require('axios');
const moment = require('moment');
const fs = require('fs');

class DataSource {
	static cacheResults(data){
		if(data) return fs.writeFileSync(Dirname + '/data/cached.json', JSON.stringify(data));
		const cachedFile = fs.readFileSync(Dirname + '/data/cached.json');
		if(cachedFile.toString('hex')){
			const cachedResult = JSON.parse(cachedFile);
			if(!moment(cachedResult.cacheTime).isAfter(moment(cachedResult.cacheTime).add(5, 'minutes'))){
				return cachedResult;
			}
		}
	}
	static get(){
		const cachedResult = DataSource.cacheResults();
		if(cachedResult) return cachedResult.result;
		return axios.get(Env.DATASOURCE).then(result => {
			DataSource.cacheResults({ result : result.data, cacheTime : new Date()});
			return result.data;
		});
	}
}

module.exports = { DataSource };