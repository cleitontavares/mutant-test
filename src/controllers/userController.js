/**
 *
 * @param app
 * @returns {{UserController: UserController}}
 */
module.exports = app => {

	const { User } = app.src.models.index;

	class UserController {
		/**
		 * static async get
		 * @param req
		 * @param res
		 * @returns {Promise<*>}
		 */
		static async get(req, res) {
			try {
				const users = await User.getAll({ filter : req.query.filter });
				return res.json(User.response({data: users}));
			} catch (e) {
				return res.status(500).json(User.response({type: 'error', message: e.message}));
			}
		}

		/**
		 * static async getSites
		 * @param req
		 * @param res
		 * @returns {Promise<*>}
		 */
		static async getSites(req, res){
			try {
				const sites = await User.getOnly('website');
				return res.json(User.response({ data : sites }));
			} catch (e) {
				return res.status(500).json(User.response({type: 'error', message: e.message}));
			}
		}

		/**
		 * static async getSuite
		 * @param req
		 * @param res
		 * @returns {Promise<*>}
		 */
		static async getSuites(req, res){
			try {
				const users = await User.getAll({ original : true });
				let result = [];
				users.forEach(user => { if(user.address.suite.indexOf('Suite') > -1) result.push(new User(user)); });
				return res.json(User.response({ data : result }));
			} catch (e) {
				return res.status(500).json(User.response({type: 'error', message: e.message}));
			}
		}
	}

	return { UserController };

};