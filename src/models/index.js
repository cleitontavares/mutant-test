const { Address } = require('./addressModel');
const { User } = require('./userModel');

module.exports = {
	Address,
	User
};