class Address {
	constructor({ street, suite, city, zipcode, geoLat, geoLng }){
		this.address = {
			street : street,
			suite : suite,
			city : city,
			zipcode : zipcode,
			geo : {
				lat : geoLat,
				lng : geoLng
			}
		}
	}
}

module.exports = { Address };