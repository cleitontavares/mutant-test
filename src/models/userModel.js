const _ = require('underscore');
const { DataSource } = require('./../dataSource');

class User{
	constructor({ name, email, company}){
		this.name = name;
		this.email = email;
		this.company = company.name;
	}
	static async getAll({ filter, original }){
		const data = await DataSource.get();
		if(original) return _.sortBy(data, filter);
		let result = [];
		data.forEach(user => {
			result.push(new User(user));
		});
		return _.sortBy(result, filter);
	}
	static async getOnly(field){
		const data = await DataSource.get();
		let result = [];
		data.forEach(item => {
			result.push(item[field]);
		});
		return result;
	}
	static response({ type, message, data }){
		if(type === 'error') return { success : false, message : message };
		return { success: true, message : message, data : data };
	}
}

module.exports = { User };