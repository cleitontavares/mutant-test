/** @namespace app.src.controllers.userController */

module.exports = app => {

	const { UserController } = app.src.controllers.userController;

	app.get('/api/sites', UserController.getSites);
	app.get('/api/users', UserController.get);
	app.get('/api/users/suite', UserController.getSuites);

	const port = Env.PORT || 8080;

	app.listen(port, () => {
		console.log('API running in port - ' + port);
	});
};