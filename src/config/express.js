const cors = require('cors');
const helmet = require('helmet');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');

module.exports = app => {

	app.set('port', process.env.PORT);
	app.set('json spaces', 4);
	app.use(helmet());
	app.use(cors());
	app.use(bodyParser.json());
	app.use(methodOverride());
	app.set('trust proxy', function (ip) {
		console.log(ip);
		return true;
		// if (ip === '127.0.0.1' || ip === '123.123.123.123') return true; // trusted IPs
		// else return false;
	});
	// app.use((req, res, next) => {
	// 	delete req.body.id;
	// 	next();
	// });
};
