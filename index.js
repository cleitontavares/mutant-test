const express = require('express');
const consign = require('consign');
const dotenv  = require('dotenv');
const { EventEmitter } = require('events');

EventEmitter.prototype._maxListeners = 0;
global.Env = process.env;
global.Dirname = __dirname;

const app = express();
dotenv.config();

consign({verbose : false, locale: 'pt-br'})
	.then('src/dataSource.js')
	.then('src/config')
	.then('src/models')
	.then('src/controllers')
	.then('src/routes.js')
	.into(app);

module.exports = app;